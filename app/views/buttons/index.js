import React, { Component, useState } from 'react';
import { 
    View, StyleSheet, Text, TextInput, TouchableHighlight, TouchableOpacity, Image,  ImageBackground,
} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { BleManager } from 'react-native-ble-plx';
 
 
function Buttons() {
 
 
    return(
        <ImageBackground source={require('../../assets/bg3.png')} style={styles.container}>
            
            
            <Text style={styles.title}>Selecciona uno de las diferentes estados</Text>
            <View style={styles.containerMid}>
           
                <TouchableHighlight
                    style={styles.btn}
                    onPress={()=> {
                        console.log("clasico" );
                    }}
                >    
                    
                    <Text style={styles.title1}>Clasico</Text>
                </TouchableHighlight>
                    
                <TouchableHighlight
                    style={styles.btn}
                    onPress={()=> {
                        console.log("Musica" );
                    }}
                >
                    <Text style={styles.title1}>Musica</Text>
                </TouchableHighlight>

                <TouchableHighlight
                    style={styles.btn}
                    onPress={()=> {
                        console.log("Sacudo" );
                    }}
                >
                    <Text style={styles.title1}>Sacudo</Text>
                </TouchableHighlight>

                <TouchableHighlight
                    style={styles.btn}
                    onPress={()=> {
                        console.log("Interactivo ");
                    }}
                >
                    <Text style={styles.title1}>Interactiv</Text>
                </TouchableHighlight>
            </View>
            <View style={styles.containerMid2}>

                <TouchableOpacity
                    style={styles.btn2}
                    onPress={()=> {
                        console.log("Modo1");
                    }}
                >
                    <Image style={styles.icon}
                        source={require('../../assets/icons/icon1.png')}
                    />
                
                </TouchableOpacity>

                <TouchableOpacity
                    style={styles.btn2}
                    onPress={()=> {
                        console.log("Modo2");
                    }}
                >
                    <Image style={styles.icon}
                        source={require('../../assets/icons/icon2.png')}
                    />
                
                </TouchableOpacity>

                <TouchableOpacity
                    style={styles.btn2}
                    onPress={()=> {
                        console.log("Modo3");
                    }}
                >
                    <Image styles={styles.icon}
                        source={require('../../assets/icons/icon3.png')}
                    />
                    
                </TouchableOpacity>
            
            </View>
            <View style={styles.containermodo1}>
                <Text style={styles.title2}>Modo 1</Text>
                <Text style={styles.title2}>Modo 2</Text>
                <Text style={styles.title2}>Modo 3</Text>
            </View>
            <View style={styles.containerMid3}>
                <TouchableOpacity
                    style={styles.btn2}
                    onPress={()=> {
                        console.log("Modo4");
                    }}
                >
                    <Image styles={styles.icon}
                        source={require('../../assets/icons/icon4.png')}
                    />
                    
                </TouchableOpacity>

                <TouchableOpacity
                    style={styles.btn2}
                    onPress={()=> {
                        console.log("Modo5");
                    }}
                ><Image styles={styles.icon}
                source={require('../../assets/icons/icon5.png')}
            />

                    
                </TouchableOpacity>

                <TouchableOpacity
                    style={styles.btn2}
                    onPress={()=> {
                        console.log("Modo6");
                    }}
                >
                    <Image styles={styles.icon} 
                        source={require('../../assets/icons/icon6.png')}
                    />
                
                </TouchableOpacity>
            </View>
            <View style={styles.containermodo2}>
                <Text style={styles.title2}>Modo 4</Text>
                <Text style={styles.title2}>Modo 5</Text>
                <Text style={styles.title2}>Modo 6</Text>
            </View>
            <View style={styles.containerMid4}>
                
                <TouchableOpacity
                    style={styles.btn2}
                    onPress={()=> {
                        console.log("Modo7");
                    }}
                >
                    <Image styles={styles.icon}
                        source={require('../../assets/icons/icon7.png')}
                    />
                    
                </TouchableOpacity>

                <TouchableOpacity
                    style={styles.btn2}
                    onPress={()=> {
                        console.log("Modo8");
                    }}
                >
                    <Image styles={styles.icon}
                source={require('../../assets/icons/icon8.png')}
            />

                
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.btn2}
                    onPress={()=> {
                        console.log("Modo9");
                    }}
                >
                    <Image styles={styles.icon}
                        source={require('../../assets/icons/icon9.png')}
                    />
                    
                </TouchableOpacity>
           </View>
          
            <View style={styles.containermodo3}>
                <Text style={styles.title2}>Modo 7</Text>
                <Text style={styles.title2}>Modo 8</Text>
                <Text style={styles.title2}>Modo 9</Text>
            </View>
           
        </ImageBackground>
        );
}
 
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        flexDirection: "column",
        height: hp('100%'),
        width: wp('100%'),
    },



    containerMid: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginLeft: '4%',
        marginTop: '1%',
        width: '90%',
        height: '-1%',
     },

    containerMid2: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: '-12%',
        width: '90%',
        height: '15%',
        marginLeft: '5%',
    },


    containerMid3: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: '4%',
        width: '90%',
        height: '15%',
        marginLeft: '5%',
    },


    containerMid4: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: '4%',
        width: '90%',
        height: '15%',
        marginLeft: '5%',
    },

    
    containermodo1: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginLeft: '8%',
        marginTop: '0%',
        height: '3%',
        width: '100%',
    },


    containermodo2: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginLeft: '8%',
        height: '3%',
        width: '100%',
        marginBottom: '0%',


    },
    containermodo3: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginLeft: '8%',
        height: '3%',
        width: '100%',
        marginTop: '1%',
    },


     icon: {
        width: '70%',
        height: '70%',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: '-1%',
    },


    flecha: {
        flex: 1,
        resizeMode: "cover",
    },

   
    title0: {
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold',
        marginLeft: '10%',
    },


    title: {
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold',
        marginVertical: '1%',
        marginLeft: '10%',   
    },

    title1: {
        color: 'black',
        fontSize: 20,
        
    },

    title2: {
        height: '100%',
        width: '32%',
        color: 'white',
        fontSize: 18,
 
        },
        
    btn: {
        borderWidth: 1,
        borderColor: 'black',
        height: '30%',
        width: '25%',
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: '1%',
        borderRadius: 20,
    },


    btn2: {
        borderWidth: 2,
        borderColor: '#623875' ,
        height: '90%',
        width: '25%',
        alignItems: 'center',
        backgroundColor: 'white',
        justifyContent: 'center',
        marginBottom: '20%',
        borderRadius: 50,
    },

}); 
 
export default Buttons;