import React, { Component, useState } from 'react';
import { 
    View, StyleSheet, Text, TextInput, TouchableOpacity, Image, ImageBackground,
} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

 
class CreateUser extends Component {
    constructor(props){
        super(props);
        this.state = {
            Nombre: null,
            Email: null,
            Password: null,
        }    
    }

    render() {
        const{Email, Password, loading} = this.state;

    return(
        <KeyboardAwareScrollView>
            <ImageBackground source={require('../../assets/bg.png')} style={styles.container}>
                

                <View style={styles.containerMid}>
                <Text style={styles.title}>Nombre</Text>
                <TextInput 
                    style={styles.text}
                    placeholder= "Coloca tu nombre completo"
                    placeholderTextColor= 'white'
                />
                <Text style={styles.title}>Email</Text>
                <TextInput 
                    style={styles.text}
                    placeholder= "Ingresa tu Email"
                    placeholderTextColor= 'white'

                />
                <Text style={styles.title}>Contraseña</Text>
                
                <TextInput 
                    secureTextEntry
                    style={styles.text}
                    placeholder= "Ingresa una contraseña"
                    placeholderTextColor= 'white'
                />
                </View>
            
                <TouchableOpacity
                    style={styles.btn}
                    onPress={()=> {
                        
                    }}
                >
                    <Text style={styles.title2}>REGISTRAR</Text>
                </TouchableOpacity>
            </ImageBackground>
        </KeyboardAwareScrollView>
    );
}
}
 
const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: hp('100%'),
        width: wp('100%') 
    },
    containerMid: {
        marginLeft:'5%',
        marginRight: '5%',
        marginTop: '13%',
    },
    title: {
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold',
        marginVertical: '2%',
        
        
    },
    title2: {
        color: 'black',
        fontSize: 24,
        fontWeight: 'bold',
        marginVertical: '1%',
    },

    text: {
        backgroundColor: '#FFFFFF24',
        height: '14%',
        width: '100%',
        paddingHorizontal: '4%',
        opacity: 0.9,
        color: 'white',
        fontSize: 18,
        
    }, 
    btn: {
        borderWidth: 1,
        borderColor: 'white',
        height: '10%',
        width: '90%',
        marginTop: '5%',
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: '5%',
        borderRadius: 2,
        fontWeight: 'bold',
        fontFamily: 'Raleway',
        
    },
});
 
export default CreateUser;