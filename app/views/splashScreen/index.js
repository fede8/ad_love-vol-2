import React, { Component, useState } from 'react';
import { 
    View, StyleSheet, Text, TextInput, TouchableOpacity, Image, ImageBackground, 
} from 'react-native';
import { NavigationEvents } from 'react-navigation';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

 
 
 
class SplashScreen extends Component {
    constructor(props){
        super(props);
        this.state = {
            Nombre: null,
            Email: null,
            Password: null,
        }    
    }

    render() {
        const{Email, Password, loading} = this.state;


        return(
        <ImageBackground source={require('../../assets/bg.png')} style={styles.container}>
            
            <View style={styles.containerSup}>
                <Image
                    source={require('../../assets/icons/icon.png')}
                />
            </View>
            <View style={styles.containerSup2}>
                <Image
                    source={require('../../assets/icons/logo.png')}
                />
            </View>    

            <TouchableOpacity
                style={styles.btn}
                onPress={() => {
                    this.props.navigation.navigate('WelcomeScreen');
                }}
                
                    
            >
             <Text style={styles.title2}>COMENZAR</Text>
            </TouchableOpacity>
            
        </ImageBackground>
        );
            }
        }


const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: hp('100%'),
        width: wp('100%'), 
        flexDirection: "column",
    },
    containerSup:{
        flexDirection: "row",
        marginLeft: '37%',
        marginTop: '28%',
    },
    containerSup2:{
        flexDirection: "row",
        marginLeft: '20%',
        marginTop: '1%',
    },
    title2: {
        color: 'white',
        fontSize: 24,
        marginVertical: '1%',
    },
    btn: {
        borderWidth: 1,
        borderColor: 'white',
        height: '10%',
        width: '90%',
        marginTop: '30%',
        backgroundColor: '#FFFFFF24',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: '5%',
        opacity: 0.9,
        borderRadius: 1,
    },
});
 
export default SplashScreen;