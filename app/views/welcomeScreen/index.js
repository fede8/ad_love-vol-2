import React, { Component, useState } from 'react';
import { 
    View, StyleSheet, Text, TextInput, TouchableOpacity, Image, ImageBackground,
} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
 
class WelcomeScreen extends Component {
    constructor(props){
        super(props);
        this.state = {
            Nombre: null,
            Email: null,
            Password: null,
        }    
    }

    render() {
        const{Email, Password, loading} = this.state;


 
    return(
        <KeyboardAwareScrollView>
            <ImageBackground source={require('../../assets/bg2.png')} style={styles.container}>
                
                <View style={styles.containerSup}>
                    <Image
                        source={require('../../assets/icons/icon.png')}
                    />
                </View>
                <View style={styles.containerSup2}>
                    <Text style={styles.titleTop}>Bienvenido</Text>
                </View>
                <View style={styles.containerMid}>
                    <TextInput 
                        style={styles.text}
                        placeholder= "Email"
                        placeholderTextColor= 'white'
                    />
                </View>
                <View style={styles.containerMid2}>
                    <TextInput 
                        style={styles.text}
                        placeholder= "Contraseña"
                        placeholderTextColor= 'white'
                    />
                </View>
                <View style={styles.containerBot2}>
        
                   
                    <TouchableOpacity
                    onPress={() => {
                        this.props.navigation.navigate('Example')
                    }}
                    >
                    <Text style={styles.titlePass}>¿Olvidaste tu contraseña?</Text>
                    </TouchableOpacity>
                </View>
            
                <TouchableOpacity
                    style={styles.btn}
                    onPress={() => {
                        this.props.navigation.navigate('AddDevice');
                    }}       
                    >
                    <Text style={styles.titleBtn}>ENTRAR</Text>
                </TouchableOpacity>

                    <View style={styles.containerBot}>
                    <TouchableOpacity
                    onPress={() => {
                        this.props.navigation.navigate('CreateUser')
                    }}
                    >

                        <Text style={styles.titleBot}>REGISTRATE</Text>
                    </TouchableOpacity>
                    </View>

            </ImageBackground>
            </KeyboardAwareScrollView>
        );
}
}
const ProfileScreen = ({ navigation, route }) => {
    return <Text>This is {route.params.name}'s profile</Text>;
  };
const styles = StyleSheet.create({
   
    container: {
       flex: 1,
       height: hp('100%'),
       width: wp('100%')
    },
    containerSup:{
        flexDirection: "column",
        marginLeft: '37%',
        marginTop: '10%',
    },
    containerSup2:{
        marginTop: '10%',
        marginLeft: '25%',
    },
    containerMid: {
        marginLeft: '5%',
        marginTop: '3%',
        width: '100%',        
    },
    containerMid2: {
        margin: '5%',
        marginTop: '-30%',
        width: '100%',
    },
    containerBot: {
        marginTop: '7%',
        marginLeft: '-3%',
    },
    containerBot2: {
        marginTop: '-40%',
    },
    titleTop: {
        color: 'white',
        fontSize: 24,
        fontWeight: 'bold',
        marginLeft: '14%',
    },
    titleBtn: {
        color: 'black',
        fontSize: 24,
        fontWeight: 'bold',
        marginVertical: '1%',
    },
    titlePass: {
        color: 'white',
        fontSize: 20,
        marginLeft: '10%',
    },
    titleBot: {
        color: 'white',
        fontSize: 20,
        marginVertical: '10%',
        marginLeft: '37%',
    },
    placeholder: {
        color: 'white',
    },
    text: {
        backgroundColor: '#FFFFFF24',
        height: '30%',
        width: '90%',
        paddingHorizontal: '5%',
        opacity: 0.9,
        color: 'white',
        fontSize: 25,
        paddingLeft: '8%',
        }, 
    btn: {
        borderWidth: 1,
        borderColor: 'white',
        height: '10%',
        width: '90%',
        marginTop: '15%', 
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: '5%',
        borderRadius: 1,
        fontWeight: 'bold',
        fontFamily: 'Raleway',
    },
});
 
export default WelcomeScreen;