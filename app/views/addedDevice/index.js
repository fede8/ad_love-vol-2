import React, { Component, useState } from 'react';
import { 
    View, StyleSheet, Text, TextInput, TouchableOpacity, Image, ImageBackground, Platform,
} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
 
 
class AddedDevice extends Component {
    constructor(props){
        super(props);
        this.state = {
            Nombre: null,
            Email: null,
            Password: null,
        }    
    }

render() {
        const{Email, Password, loading} = this.state;

    return(
        <ImageBackground source={require('../../assets/bgsimple.png')} style={styles.container}>
            
            <View style={styles.containerAgr}>
            <TouchableOpacity
                style={styles.btn}
                onPress={() => {
                    this.props.navigation.navigate('Buttons')
                }}
            >
                 <Image
                    source={require('../../assets/icons/check.png')}
                />
               
            </TouchableOpacity>
            </View>
            
            <View style={styles.containerText1}>
            <Text style={styles.text1}>Dispositivo Agregado</Text>
            </View>
            <View style={styles.containerText2}>
            <Text style={styles.text2}>
            Su dispositivo se agrego satisfactoriamente {' \n'}
             continua para poder acceder
             </Text>
           </View>
        </ImageBackground>
        );
}
}
 
const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: hp('100%'),
        width: wp('100%'),
        flexDirection: "column",
    },
    containerAgr: {
        justifyContent:'center',
        alignItems:'center',
        width: '30%',
        height:'30%',
        marginLeft: '38%' ,
        marginRight: '38%',
        marginTop: '35%',
        opacity: 1,
    },
    containerText1: {
        marginLeft: '28%',
        marginRight: '25%',
        marginTop: '1%',
        height: '5%',
        width: '100%',
    },
    containerText2: {
         marginLeft: '13%',
         marginRight: '15%',
         marginTop: '4%',
         width: '79%',
         height: '15%',
    },
    text1: {
        fontFamily: 'Raleway-SemiBold',
        color: "#FFF",
         fontSize: 20,
         textAlign: 'left',
       }, 
    text2: {
        color: "#FFFFFF",
        fontFamily: 'Raleway-Light',
        fontSize: 14,
        textAlign: 'center',
    },
  
});
 
export default AddedDevice;