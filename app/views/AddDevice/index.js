import React, { Component, useState } from 'react';
import { 
    View, StyleSheet, Text, TextInput, TouchableOpacity, Image, ImageBackground,
} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

class AddDevice extends Component {
    constructor(props){
        super(props);
        this.state = {
            Nombre: null,
            Email: null,
            Password: null,
        }    
    }

render() {
        const{Email, Password, loading} = this.state;

return(
    <ImageBackground source={require('../../assets/bgsimple.png')} style={styles.container}>
            
        <View style={styles.containerSup}>
            <Image style={styles.profileimage}
                source={require('../../assets/icons/user-alt.png')}
                
            />
            <TouchableOpacity
            style={styles.btnprofile}
            onPress={()=> {
                console.log({Profile });
            }}
            >
            
            </TouchableOpacity>
            <View style={styles.logo}>
                <Image
                    source={require('../../assets/logo.png')}
                />
            </View>
        </View>

        <View style={styles.containerMid}>
        
            <Image style={styles.btngrande}
                source={require('../../assets/icons/glow-buttom.png')}

            />
            <TouchableOpacity
            style={styles.btnadd}
            onPress={()=> {
                this.props.navigation.navigate('AddedDevice')
            }}
            >
                <Image 
                source={require('../../assets/add.png')}
                />
            </TouchableOpacity>
        </View>   
                
        <Text style={styles.text}>Agregar dispositivo</Text>
        <Text style={styles.text2}>Controla tu dispositivo con tu dispositivo movil</Text>
            
    </ImageBackground>  
    );
}
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: hp('100%'),
        width: wp('100%'),
        flexDirection: "column",
    },
    containerSup:{
        flexDirection: "row-reverse",
        marginLeft:'5%',
        marginTop: '5%',
    },
    containerMid: {
        marginLeft:'9%',
        marginTop: '20%',
    },
    logo: {
        marginLeft:'5%',
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start', //replace with flex-end or center
    },
    text: {
        color: '#FFFFFF',
        fontSize: 20,        
        fontFamily:'Raleway-Semibold',
        textAlign:'center'
    },
    text2:{
        color: '#FFFFFF',
        textAlign:'center',
        fontSize: 14,
        justifyContent: 'center',
        marginVertical: '5%',
        width: '100%',
    },
    btnprofile:{
        marginLeft:'-100%',
        marginTop: '-2%',
        marginRight: '-13%',
        borderRadius: 8,
        height: '110%',
        width: '15%',
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: 'white',
        backgroundColor:'#FFFFFF30'
    },
    btnadd:{
        height:'60%',
        width:'80%',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: '-73%',
        marginLeft: '9%' ,
    },
    btngrande:{
        marginLeft: '3%',
        marginTop: '-20%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    profileimage:{
        marginLeft:'4%',
        marginTop: '1%',
        marginRight: '1%',
        flexDirection: "row-reverse",
        height: '60%',
        width: '8%',
    }
});
 
export default AddDevice;