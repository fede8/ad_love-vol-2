import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';

//screens 

import Routes from './stackRoutes';

const Stack = createStackNavigator();


function AppStack() {
  return (
      <NavigationContainer>
      <Stack.Navigator>
            <Stack.Screen 
            name="SplashScreen" 
            component={Routes.SplashScreen} 
            options={{
                headerShown: false,
            }}
            />
            <Stack.Screen 
            name="WelcomeScreen" 
            component={Routes.WelcomeScreen}
            options={{
              headerShown: false,
            }} 
            />
            <Stack.Screen
            name="AddDevice"
            component={Routes.AddDevice}
            options={{
              title: 'AddDevice',
              headerShown: false,
            }}
            />
            <Stack.Screen
            name="AddedDevice"
            component={Routes.AddedDevice}
            options={{
              title: 'Regresar',
              headerTransparent: true,
              headerTintColor: 'white'
            }}
            />
              <Stack.Screen
            name="Buttons"
            component={Routes.Buttons}
            options={{
              title: 'Regresar',
              headerTransparent: true,
              headerTintColor: 'white'
            }}
            />
            <Stack.Screen
            name="CreateUser"
            component={Routes.CreateUser}
            options={{
              title: 'Registro',
              headerTransparent: true,
              headerTintColor: 'white'
            }}
            />
             <Stack.Screen
            name="Example"
            component={Routes.Example}
            options={{
              title: 'Ejemplo',
              headerTransparent: true,
              headerTintColor: 'white'
            }}
            />
            

          
      </Stack.Navigator>
      </NavigationContainer>
  );
}

export default AppStack;