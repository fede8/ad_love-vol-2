import SplashScreen from '../views/splashScreen';
import WelcomeScreen from '../views/welcomeScreen';
import CreateUser from '../views/createUser';
import AddDevice from '../views/addDevice'
import AddedDevice from '../views/addedDevice';
import Buttons from '../views/buttons';
import Example from '../views/example/index';

export default {
    SplashScreen,
    WelcomeScreen,
    CreateUser,
    AddDevice,
    AddedDevice,
    Buttons,
    Example,
}